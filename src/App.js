import React from "react";
import { Router, Route, Switch } from "react-router-dom";
import history from "./history";

import Login from "./pages/Login";
import ForgotPassword from "./pages/ForgotPassword";
import ResetPassword from "./pages/ResetPassword";
import { ROUTE } from "./constants/routeConstants";
import PageContainer from "./pages/PageContainer";
import NotFound from "./components/NotFound";
import ProtectedRoute from "./components/ProtectedRoute";

const App = () => {
	return (
		<Router history={history}>
			<div className="full-width-height">
				<Switch>
					<Route path={ROUTE.LOGIN} exact component={Login} />
					<Route
						path={ROUTE.FORGOT_PW}
						exact
						component={ForgotPassword}
					/>
					<Route
						path={ROUTE.RESET_PW}
						exact
						component={ResetPassword}
					/>

					<ProtectedRoute
						path={ROUTE.APP}
						component={PageContainer}
					/>

					<Route path="*" component={NotFound} />
				</Switch>
			</div>
		</Router>
	);
};

export default App;
