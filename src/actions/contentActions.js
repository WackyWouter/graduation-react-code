import { fakeCms } from "../api/fakeCms";
import { KEY_UPDATE } from "../constants/userConstants";
import {
	APP_ID,
	CLEAR_ERROR,
	CLEAR_SEARCH,
	DIVISION_ID,
	FEEDBACK_SEEN,
	GET_ACTIVITIES,
	GET_FEEDBACK,
	GET_HOURS,
	GET_LEADERBOARD,
	GET_PROJECTS,
	GET_USER_INFO,
	SET_ERROR,
	SET_SEARCH,
	SORT_PROJECTS,
	STANDERD_ERROR,
} from "../constants/contentConstants";
import { formatDate } from "../helperFunctions/Date";
import history from "../history";
import { ROUTE } from "../constants/routeConstants";

export const getProjects = (userKey) => async (dispatch) => {
	await fakeCms
		.post("/get_projects.php", {
			app: APP_ID,
			division_id: DIVISION_ID,
			key: userKey,
		})
		.then((response) => {
			if (typeof response.data.key !== "undefined") {
				// Sort project a to z;
				const projects = response.data.data;
				projects.sort(function (a, b) {
					const aProject = a.project.toLowerCase();
					const bProject = b.project.toLowerCase();

					if (aProject < bProject) {
						return -1;
					}
					if (aProject > bProject) {
						return 1;
					}
					return 0;
				});

				// update key in redux store
				localStorage.setItem("key", response.data.key);
				dispatch({ type: KEY_UPDATE, payload: response.data.key });

				dispatch({ type: GET_PROJECTS, payload: projects });
			} else {
				dispatch({
					type: SET_ERROR,
					payload: STANDERD_ERROR,
				});
			}
		})
		.catch((error) => {
			dispatch({
				type: SET_ERROR,
				payload: error.toString(),
			});
		});
};

export const sortProjects = (projects, aToZ) => (dispatch) => {
	// Sort from a to z or from z to a
	projects.sort(function (a, b) {
		const aProject = a.project.toLowerCase();
		const bProject = b.project.toLowerCase();

		if (aProject < bProject) {
			return aToZ ? 1 : -1;
		}
		if (aProject > bProject) {
			return aToZ ? -1 : 1;
		}
		return 0;
	});

	dispatch({
		type: SORT_PROJECTS,
		payload: {
			projects: [...projects],
			sort: !aToZ,
		},
	});
};

export const clearSearch = () => (dispatch) => {
	dispatch({ type: CLEAR_SEARCH });
};

export const clearError = () => (dispatch) => {
	dispatch({ type: CLEAR_ERROR });
};

export const searchProjects = (searchTerm, projects) => (dispatch) => {
	// Search the projects array for matching cases
	projects = projects.filter((project) => {
		if (project.project.toLowerCase().includes(searchTerm.toLowerCase())) {
			return true;
		}
		return false;
	});

	dispatch({
		type: SET_SEARCH,
		payload: {
			search: searchTerm,
			projects: [...projects],
		},
	});
};

export const getActivities = (userKey) => async (dispatch) => {
	await fakeCms
		.post("/get_activities.php", {
			app: APP_ID,
			division_id: DIVISION_ID,
			key: userKey,
		})
		.then((response) => {
			if (typeof response.data.key !== "undefined") {
				// update key in redux store and update content
				localStorage.setItem("key", response.data.key);
				dispatch({ type: KEY_UPDATE, payload: response.data.key });

				dispatch({ type: GET_ACTIVITIES, payload: response.data.data });
			} else {
				dispatch({
					type: SET_ERROR,
					payload: STANDERD_ERROR,
				});
			}
		})
		.catch((error) => {
			dispatch({
				type: SET_ERROR,
				payload: error.toString(),
			});
		});
};

export const getHours = (userKey) => async (dispatch) => {
	await fakeCms
		.post("/get_hours.php", {
			app: APP_ID,
			division_id: DIVISION_ID,
			key: userKey,
			week: 15,
			year: 2021,
		})
		.then((response) => {
			if (typeof response.data.key !== "undefined") {
				// update key in redux store and update content
				localStorage.setItem("key", response.data.key);
				dispatch({ type: KEY_UPDATE, payload: response.data.key });

				dispatch({ type: GET_HOURS, payload: response.data.data });
			} else {
				dispatch({
					type: SET_ERROR,
					payload: STANDERD_ERROR,
				});
			}
		})
		.catch((error) => {
			dispatch({
				type: SET_ERROR,
				payload: error.toString(),
			});
		});
};

export const getFeedback = (userKey) => async (dispatch) => {
	await fakeCms
		.post("/get_feedback.php", {
			app: APP_ID,
			division_id: DIVISION_ID,
			key: userKey,
		})
		.then((response) => {
			if (typeof response.data.key !== "undefined") {
				// update key in redux store and update content
				localStorage.setItem("key", response.data.key);
				dispatch({ type: KEY_UPDATE, payload: response.data.key });

				dispatch({ type: GET_FEEDBACK, payload: response.data.data });
			} else {
				dispatch({
					type: SET_ERROR,
					payload: STANDERD_ERROR,
				});
			}
		})
		.catch((error) => {
			dispatch({
				type: SET_ERROR,
				payload: error.toString(),
			});
		});
};

export const askForMoreFeedback = (userKey) => async (dispatch) => {
	await fakeCms
		.post("/post_moreFeedback.php", {
			app: APP_ID,
			division_id: DIVISION_ID,
			key: userKey,
		})
		.then((response) => {
			if (typeof response.data.key !== "undefined") {
				// update key in redux store and update content
				localStorage.setItem("key", response.data.key);
				dispatch({ type: KEY_UPDATE, payload: response.data.key });

				history.push(ROUTE.HOME);
			} else {
				dispatch({
					type: SET_ERROR,
					payload: STANDERD_ERROR,
				});
			}
		})
		.catch((error) => {
			dispatch({
				type: SET_ERROR,
				payload: error.toString(),
			});
		});
};

export const setFeedbackSeen = (userKey, oldFeedback) => async (dispatch) => {
	await fakeCms
		.post("/post_SetFeedback.php", {
			app: APP_ID,
			division_id: DIVISION_ID,
			key: userKey,
			feedback_id: oldFeedback.id,
		})
		.then((response) => {
			if (typeof response.data.key !== "undefined") {
				// update key in redux store and update content
				localStorage.setItem("key", response.data.key);
				dispatch({ type: KEY_UPDATE, payload: response.data.key });

				// Set feedback to seen in redux store
				const feedback = { ...oldFeedback, notSeen: false };

				dispatch({ type: FEEDBACK_SEEN, payload: feedback });
			} else {
				dispatch({
					type: SET_ERROR,
					payload: STANDERD_ERROR,
				});
			}
		})
		.catch((error) => {
			dispatch({
				type: SET_ERROR,
				payload: error.toString(),
			});
		});
};

export const getLeaderboard = (userKey) => async (dispatch) => {
	await fakeCms
		.post("/get_leaderboard.php", {
			app: APP_ID,
			division_id: DIVISION_ID,
			key: userKey,
		})
		.then((response) => {
			if (typeof response.data.key !== "undefined") {
				// update key in redux store and update content
				localStorage.setItem("key", response.data.key);
				dispatch({ type: KEY_UPDATE, payload: response.data.key });

				dispatch({
					type: GET_LEADERBOARD,
					payload: response.data.data,
				});
			} else {
				dispatch({
					type: SET_ERROR,
					payload: STANDERD_ERROR,
				});
			}
		})
		.catch((error) => {
			dispatch({
				type: SET_ERROR,
				payload: error.toString(),
			});
		});
};

export const getUserInfo = (userKey) => async (dispatch) => {
	await fakeCms
		.post("/get_userinfo.php", {
			app: APP_ID,
			division_id: DIVISION_ID,
			key: userKey,
		})
		.then((response) => {
			if (typeof response.data.key !== "undefined") {
				// update key in redux store and update content
				localStorage.setItem("key", response.data.key);
				dispatch({ type: KEY_UPDATE, payload: response.data.key });

				dispatch({ type: GET_USER_INFO, payload: response.data.data });
			} else {
				dispatch({
					type: SET_ERROR,
					payload: STANDERD_ERROR,
				});
			}
		})
		.catch((error) => {
			dispatch({
				type: SET_ERROR,
				payload: error.toString(),
			});
		});
};

export const postHours =
	(oldHourArray, formValues, key, hoursId = 0) =>
	async (dispatch) => {
		await fakeCms
			.post("/post_hours.php", {
				hours_id: hoursId, // zero to add new items
				app: APP_ID,
				division_id: DIVISION_ID,
				key: key,
				project_id: formValues.project,
				activity_id: formValues.activity,
				registration_date: formatDate(new Date()),
				start_datetime: formValues.date + " " + formValues.startTime,
				end_datetime: formValues.date + " " + formValues.endTime,
				description: formValues.remarks,
			})
			.then((response) => {
				if (typeof response.data.key !== "undefined") {
					// update key in redux store and update content
					localStorage.setItem("key", response.data.key);

					dispatch({ type: KEY_UPDATE, payload: response.data.key });

					// ADD to redux store also
					var highestId = 0;
					oldHourArray.forEach((hour) => {
						if (hour.id > highestId) {
							highestId = hour.id;
						}
					});

					var newHourArray = [];

					if (hoursId === 0) {
						newHourArray = [
							{
								id: highestId + 1,
								activity_id: parseInt(formValues.activity),
								project_id: parseInt(formValues.project),
								user_id: 1,
								description: formValues.remarks,
								end_datetime:
									formValues.date + " " + formValues.endTime,
								start_datetime:
									formValues.date +
									" " +
									formValues.startTime,
								locked: false,
								registration_date: formatDate(new Date()),
							},
							...oldHourArray,
						];
					} else {
						// replace one item and return new array
						newHourArray = oldHourArray.map((hour) => {
							if (hour.id === hoursId) {
								return {
									id: hoursId,
									activity_id: parseInt(formValues.activity),
									project_id: parseInt(formValues.project),
									user_id: 1,
									description: formValues.remarks,
									end_datetime:
										formValues.date +
										" " +
										formValues.endTime,
									start_datetime:
										formValues.date +
										" " +
										formValues.startTime,
									locked: false,
									registration_date: formatDate(new Date()),
								};
							}
							return hour;
						});
					}

					dispatch({
						type: GET_HOURS,
						payload: newHourArray,
					});

					history.push(ROUTE.WORKED_HOURS + "1");
				} else {
					dispatch({
						type: SET_ERROR,
						payload: STANDERD_ERROR,
					});
				}
			})
			.catch((error) => {
				dispatch({
					type: SET_ERROR,
					payload: error.toString(),
				});
			});
	};
