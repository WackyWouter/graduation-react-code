import {
	CLOSE_MENU,
	OPEN_MENU,
	OPEN_SUG,
	CLOSE_SUG,
	SET_TITLE,
	SET_SUG_DETAILS,
	CLEAR_SUG_DETAILS,
	ACCEPT_SUG,
} from "../constants/displayConstants";
import geonames from "../api/geonames";
import history from "../history";
import { ROUTE } from "../constants/routeConstants";

export const openMenu = () => (dispatch) => {
	dispatch({
		type: OPEN_MENU,
	});
};

export const closeMenu = () => (dispatch) => {
	dispatch({
		type: CLOSE_MENU,
	});
};

export const openSug = () => (dispatch) => {
	dispatch({
		type: OPEN_SUG,
	});
};

export const closeSug = () => (dispatch) => {
	dispatch({
		type: CLOSE_SUG,
	});
};

export const setPageTitle = (title) => (dispatch) => {
	dispatch({
		type: SET_TITLE,
		payload: title,
	});
};

export const clearSug = () => (dispatch) => {
	dispatch({
		type: CLEAR_SUG_DETAILS,
	});
};

export const acceptSug = () => (dispatch) => {
	dispatch({
		type: ACCEPT_SUG,
	});

	history.push(ROUTE.REG_HOURS);
};

export const setSugDetails =
	(projects, { latitude, longitude }) =>
	async (dispatch) => {
		const response = await geonames.get("", {
			params: {
				lat: latitude,
				lng: longitude,
			},
		});

		// check if call went correctly
		if (response.status !== 200 || !response.data.streetSegment) {
			return null;
		}

		const street = response.data.streetSegment[0].name;

		if (street) {
			// See if we can find a project with the streetname in its adress
			var project = projects.find((project) => {
				if (
					project.address.toLowerCase().includes(street.toLowerCase())
				) {
					return project;
				}
				return null;
			});

			// If we cant find one that way see if we can find one with lat and lon
			if (!project) {
				// look if there is a company within 100m radius of the users location
				// https://www.usna.edu/Users/oceano/pguth/md_help/html/approx_equivalents.htm
				const latMin = latitude - 0.001;
				const latMax = latitude + 0.001;
				const lonMin = longitude - 0.001;
				const lonMax = longitude + 0.001;
				project = projects.find((project) => {
					if (
						project.latitude > latMin &&
						project.latitude < latMax &&
						project.longitude > lonMin &&
						project.longitude < lonMax
					) {
						return project.project;
					}
					return null;
				});
			}

			// Compose a suggestion if a matching project is found and that project has activities
			if (project && project.activities.length > 0) {
				const date = new Date();
				const text = `Heeft u de taak: "${
					project.activities[0].activity
				}" gedaan voor ${
					project.project
				} op ${date.getDate()} ${date.toLocaleString("nl-nl", {
					month: "long",
				})}?`;

				dispatch({
					type: SET_SUG_DETAILS,
					payload: {
						text: text,
						projectId: project.id,
						activityId: project.activities[0].id,
					},
				});
			}
		}
	};
