import {
	SIGN_IN,
	SIGN_OUT,
	FAILURE,
	FORGOTTEN_PW,
	PASSWORD_RESET,
	CLEAR,
	SET_COORDS,
} from "../constants/userConstants";
import history from "../history";
import { fakeCms } from "../api/fakeCms";
import { ROUTE } from "../constants/routeConstants";
import {
	APP_ID,
	DIVISION_ID,
	STANDERD_ERROR,
} from "../constants/contentConstants";

export const signIn = (formValues) => async (dispatch) => {
	if (typeof formValues.remember !== "undefined") {
		localStorage.setItem("email", formValues.email);
	} else {
		localStorage.removeItem("email");
	}

	// make call to fakeCms to login
	await fakeCms
		.post("/post_login.php", {
			app: APP_ID,
			division_id: DIVISION_ID,
			username: formValues.email,
			password: formValues.password,
		})
		.then((response) => {
			if (
				typeof response.data.key !== "undefined" &&
				response.data.status === "ok"
			) {
				// store var in locale storage to remember user is authenticated
				localStorage.setItem("isAuthenticated", "true");
				localStorage.setItem("key", response.data.key);

				dispatch({ type: SIGN_IN, payload: response.data });

				// redirect to home cause logged in
				history.push(ROUTE.HOME);
			} else {
				dispatch({
					type: FAILURE,
					payload: response.data.error ?? STANDERD_ERROR,
				});
			}
		})
		.catch((error) => {
			dispatch({
				type: FAILURE,
				payload: error.toString(),
			});
		});
};

export const signInWithKey = () => async (dispatch) => {
	const key = localStorage.getItem("key");

	await fakeCms
		.post("/post_login.php", {
			app: APP_ID,
			division_id: DIVISION_ID,
			key: key,
		})
		.then((response) => {
			if (
				typeof response.data.key !== "undefined" &&
				response.data.status === "ok"
			) {
				localStorage.setItem("key", response.data.key);
				dispatch({
					type: SIGN_IN,
					payload: {
						key: response.data.key,
						streak: response.data.streak,
					},
				});
			} else {
				clearLocalStorage();
				dispatch({
					type: SIGN_OUT,
				});
			}
		})
		.catch((error) => {
			clearLocalStorage();
			dispatch({
				type: SIGN_OUT,
			});
		});
};

function clearLocalStorage() {
	localStorage.removeItem("key");
	localStorage.removeItem("isAuthenticated");
}

export const signOut = () => (dispatch) => {
	// clear local storage and sign out in redux
	clearLocalStorage();
	dispatch({
		type: SIGN_OUT,
	});
};

export const forgotPassword = (formValues) => async (dispatch) => {
	// make call to fakeCms to send email to reset email
	await fakeCms
		.post("/post_forgotpassword.php", {
			app: APP_ID,
			division_id: DIVISION_ID,
			email: formValues.email,
		})
		.then((response) => {
			if (
				typeof response.data.key !== "undefined" &&
				response.data.status === "ok"
			) {
				// redirect to login
				history.push(ROUTE.LOGIN);

				dispatch({
					type: FORGOTTEN_PW,
				});
			} else {
				dispatch({
					type: FAILURE,
					payload: response.data.error ?? STANDERD_ERROR,
				});
			}
		})
		.catch((error) => {
			dispatch({
				type: FAILURE,
				payload: error.toString(),
			});
		});
};

export const resetPw = (formvalues) => async (dispatch) => {
	if (formvalues.password !== formvalues.confirmPassword) {
		dispatch({
			type: FAILURE,
			payload: "De twee wachtwoorden moeten hetzelfde zijn!",
		});
	} else {
		// Make call to fakeCms to reset password

		await fakeCms
			.post("/post_resetpassword.php", {
				app: APP_ID,
				division_id: DIVISION_ID,
				email: formvalues.email,
				password: formvalues.password,
			})
			.then((response) => {
				if (
					typeof response.data.key !== "undefined" &&
					response.data.status === "ok"
				) {
					// store var in locale storage to remember user is authenticated
					localStorage.setItem("isAuthenticated", "true");
					localStorage.setItem("key", response.data.key);

					dispatch({ type: PASSWORD_RESET, payload: response.data });

					// redirect to home cause logged in
					history.push(ROUTE.HOME);
				} else {
					dispatch({
						type: FAILURE,
						payload: response.data.error ?? STANDERD_ERROR,
					});
				}
			})
			.catch((error) => {
				dispatch({
					type: FAILURE,
					payload: error.toString(),
				});
			});
	}
};

export const setCoords = (coords) => (dispatch) => {
	dispatch({
		type: SET_COORDS,
		payload: coords,
	});
};

// clears error
export const clear = () => (dispatch) => {
	dispatch({
		type: CLEAR,
	});
};
