import db from "./data.json";
import MockAdapter from "axios-mock-adapter";
// import faker from "faker";

export const isMockEnabled = true;

export const initializeAxiosMockAdapter = (instance) => {
	// initiate mock backend with delay
	const mock = new MockAdapter(instance, { delayResponse: 1000 });
	mock.onPost("/gateway/post_login.php").reply((config) =>
		authenticate(config)
	);

	mock.onPost("/gateway/post_login.php").reply((config) =>
		authenticate(config)
	);

	mock.onPost("/gateway/get_hours.php").reply((config) =>
		workedHours(config)
	);
	mock.onPost("/gateway/get_projects.php").reply((config) =>
		projects(config)
	);
	mock.onPost("/gateway/get_activities.php").reply((config) =>
		activities(config)
	);
	mock.onPost("/gateway/post_hours.php").reply((config) => postHours(config));
	mock.onPost("/gateway/get_leaderboard.php").reply((config) =>
		leaderboard(config)
	);
	mock.onPost("/gateway/get_feedback.php").reply((config) =>
		feedback(config)
	);
	mock.onPost("/gateway/get_userinfo.php").reply((config) =>
		userInfo(config)
	);
	mock.onPost("/gateway/post_moreFeedback.php").reply((config) =>
		feedbackChanges(config)
	);
	mock.onPost("/gateway/post_SetFeedback.php").reply((config) =>
		feedbackChanges(config)
	);
	mock.onPost("/gateway/post_forgotpassword.php").reply((config) =>
		forgotPW(config)
	);
	mock.onPost("/gateway/post_resetpassword.php").reply((config) =>
		resetPw(config)
	);
};

const authenticate = ({ data }) => {
	const param = JSON.parse(data);
	const users = db.users;
	var user = null;

	if (param.key) {
		user = users.find((user) => user.key === param.key);
	} else {
		user = users.find(
			(user) =>
				user.email === param.username &&
				user.password === param.password
		);
	}

	if (user) {
		return [
			200,
			{
				status: "ok",
				key: user.key,
				streak: user.streak,
			},
		];
	} else {
		return [
			200,
			{
				status: "nok",
				error: "user not found",
			},
		];
	}
};

const workedHours = ({ data }) => {
	const param = JSON.parse(data);
	const workedHours = db.hours;

	return [
		200,
		{
			key: param.key,
			data: workedHours,
		},
	];
};

const projects = ({ data }) => {
	const param = JSON.parse(data);
	const projects = db.projects;

	return [
		200,
		{
			key: param.key,
			data: projects,
		},
	];
};

const activities = ({ data }) => {
	const param = JSON.parse(data);
	const activities = db.activities;

	return [
		200,
		{
			key: param.key,
			data: activities,
		},
	];
};

const postHours = ({ data }) => {
	const param = JSON.parse(data);

	const lastId = db.hours[db.hours.length - 1].id;

	if (param.hours_id === 0) {
		db.hours.unshift({
			id: lastId + 1,
			activity_id: param.activity_id,
			project_id: param.project_id,
			user_id: 1,
			description: param.description,
			end_datetime: param.end_datetime,
			start_datetime: param.start_datetime,
			locked: false,
			registration_date: param.registration_date,
		});
	} else {
		db.hours[param.hours_id - 1] = {
			id: param.hours_id,
			activity_id: param.activity_id,
			project_id: param.project_id,
			user_id: 1,
			description: param.description,
			end_datetime: param.end_datetime,
			start_datetime: param.start_datetime,
			locked: false,
			registration_date: param.registration_date,
		};
	}

	return [
		200,
		{
			status: "ok",
			key: param.key,
		},
	];
};

const userInfo = ({ data }) => {
	const param = JSON.parse(data);
	const users = db.users;
	var user = null;
	console.log(param.key);
	user = users.find((user) => user.key === param.key);

	return [
		200,
		{
			key: param.key,
			data: {
				id: user.id,
				displayname: user.displayname,
				streak: user.streak,
				highest_streak: user.highest_streak,
				highest_place: user.highest_place,
				place: user.place,
				highest_feedback: user.highest_feedback,
				avatar: user.avatar,
				email: user.email,
			},
		},
	];
};

const feedback = ({ data }) => {
	const param = JSON.parse(data);
	const feedback = db.feedback;

	return [
		200,
		{
			key: param.key,
			data: feedback,
		},
	];
};
const feedbackChanges = ({ data }) => {
	const param = JSON.parse(data);
	const feedback = db.feedback;

	if (typeof param.feedback_id !== undefined) {
		feedback.notSeen = false;
	}

	return [
		200,
		{
			key: param.key,
			status: "ok",
		},
	];
};

const forgotPW = ({ data }) => {
	const param = JSON.parse(data);

	return [
		200,
		{
			key: param.key,
			status: "ok",
		},
	];
};

const resetPw = () => {
	return [
		200,
		{
			key: "24451fe6-dfcb-4676-bb4e-fff77d221d0a",
			status: "ok",
		},
	];
};

const leaderboard = ({ data }) => {
	const param = JSON.parse(data);
	const leaderboard = db.leaderboard;
	// console.log(faker.image.avatar());

	return [
		200,
		{
			key: param.key,
			data: leaderboard,
		},
	];
};

// const extractIdPathParamFromUrl = (config) => {
// 	return config.url.split("/").pop();
// };
