import axios from "axios";
import {
	initializeAxiosMockAdapter,
	isMockEnabled,
} from "./fakeBackend/mock.config";

let instance = axios.create({
	baseURL: "/gateway",
});

if (isMockEnabled) {
	initializeAxiosMockAdapter(instance);
}

export const fakeCms = instance;
