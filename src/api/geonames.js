import axios from "axios";

const geonames = axios.create({
	baseURL: "http://api.geonames.org/findNearbyStreetsOSMJSON",
	params: {
		username: "wackywouter",
	},
});

export default geonames;
