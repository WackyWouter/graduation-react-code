import "./AlertBox.css";

import React from "react";

const AlertBox = React.forwardRef(
	({ children, text, btnNum, classes }, ref) => {
		return (
			<div ref={ref} className={`alert-box shadow ${classes}`}>
				<p>{text}</p>
				<div className={btnNum}>{children}</div>
			</div>
		);
	}
);

export default AlertBox;
