import React from "react";
import history from "../history";

import BackIcon from "../assets/back-thin.svg";

const Back = () => {
	const goBack = () => {
		history.goBack();
	};
	return (
		<img src={BackIcon} className="back" onClick={goBack} alt="back-icon" />
	);
};

export default Back;
