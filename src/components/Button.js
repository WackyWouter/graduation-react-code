import "./Button.css";

import React from "react";
import { Link } from "react-router-dom";

const Button = ({ text, classes, to, onClick }) => {
	if (to) {
		return (
			<Link className={`${classes ?? ""} button shadow`} to={to}>
				{text}
			</Link>
		);
	}
	return (
		<button className={`${classes ?? ""} button shadow`} onClick={onClick}>
			{text}
		</button>
	);
};

export default Button;
