import "./Dropdown.css";

import React from "react";

const Dropdown = ({ input, options, onChange, meta: { touched, error } }) => {
	const errorClass = touched && error ? "input-error" : "";

	const renderSelectOptions = (option) => (
		<option key={option.id} value={option.id}>
			{option.project ?? option.activity}
		</option>
	);

	return (
		<div>
			<select {...input} className={errorClass}>
				<option value="">Select</option>
				{options.map(renderSelectOptions)}
			</select>
		</div>
	);
};

export default Dropdown;
