import React from "react";

import errorIcon from "../assets/error-icon.svg";

const ErrorMessage = ({ error }) => {
	if (error) {
		return (
			<div className="error-container">
				<img src={errorIcon} alt="error-icon" className="error-icon" />
				<span className="error">{error}</span>
			</div>
		);
	}
	return null;
};

export default ErrorMessage;
