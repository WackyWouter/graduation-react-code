import React from "react";

import Cool from "../assets/smiley/cool.png";
import Good from "../assets/smiley/good.png";
import Meh from "../assets/smiley/meh.png";
import Sad from "../assets/smiley/sad.png";
import Crying from "../assets/smiley/crying.png";
import Shy from "../assets/smiley/shy.png";
import StarFilled from "../assets/star-filled.svg";
import Star from "../assets/star.svg";

const FeedbackBox = ({ feedback, score }) => {
	const renderSmiley = (score) => {
		switch (score) {
			case 1: {
				return Crying;
			}
			case 2: {
				return Sad;
			}
			case 3: {
				return Meh;
			}
			case 4: {
				return Good;
			}
			case 5: {
				return Cool;
			}
			default: {
				return Shy;
			}
		}
	};

	const renderStars = (score) => {
		var stars = [];
		var i;

		for (i = 1; i < 6; i++) {
			if (i > score) {
				stars.push(<img src={Star} key={i} alt="star-icon" />);
			} else {
				stars.push(
					<img src={StarFilled} key={i} alt="filled-star-icon" />
				);
			}
		}
		return <div>{stars}</div>;
	};

	return (
		<div className="feedback-box">
			<img
				className="feedback-img"
				src={renderSmiley(score)}
				alt="emotion-smiley op basis van de score"
			/>
			{renderStars(score)}
			<div className="feedback-text-con">
				<p className="title mb-5">Feedback</p>
				<p className="feedback-text">{feedback}</p>
			</div>
		</div>
	);
};

export default FeedbackBox;
