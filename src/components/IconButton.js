import React from "react";

import { Link } from "react-router-dom";

import Exclamation from "../assets/exclamation.svg";

const IconButton = ({ to, icon, text, onClick, classes, notSeen = false }) => {
	if (to) {
		const renderExclamation = () => {
			if (notSeen) {
				return <img src={Exclamation} alt="red-exclamation-mark" />;
			}
		};
		return (
			<Link to={to}>
				<div
					className={`${classes} button white shadow`}
					onClick={onClick}
				>
					<div>
						<img
							className="button-icon"
							src={icon}
							alt="button-icon"
						/>
						<span>{text}</span>
					</div>
					{renderExclamation()}
				</div>
			</Link>
		);
	}
	return (
		<div className={`${classes} button white shadow`} onClick={onClick}>
			<div>
				<img className="button-icon" src={icon} alt="button-icon" />
				<span>{text}</span>
			</div>
		</div>
	);
};

export default IconButton;
