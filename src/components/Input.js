import React from "react";

const Input = ({ input, label, type, meta: { touched, error } }) => {
	const errorClass = touched && error ? "input-error" : "";

	switch (type) {
		case "checkbox":
			return (
				<div className="checkbox-container">
					<input
						{...input}
						id={input.name}
						type={type}
						className="checkbox"
					/>
					<label htmlFor={input.name}>{label}</label>
				</div>
			);
		default:
			return (
				<div className="input-container">
					<input
						{...input}
						placeholder={label}
						type={type}
						className={errorClass}
					/>
				</div>
			);
	}
};

export default Input;
