import "./ItemList.css";

import React from "react";
import { Link } from "react-router-dom";

import ChevronRight from "../assets/chevron-right.svg";
import { ROUTE } from "../constants/routeConstants";

const ItemList = ({ items, type }) => {
	const renderDate = (date) => {
		if (date) {
			return (
				<div className="item-date-container">
					<div className="item-row-date">{date}</div>
				</div>
			);
		}
		return null;
	};

	function createLink(name, id, route, date, classes) {
		return (
			<Link
				className={`item-row ${classes}`}
				to={route + id.toString()}
				key={id}
			>
				<div className="item-sub-row">
					{renderDate(date)}
					<div className="item-name">{name}</div>
				</div>
				<div>
					<img
						className="item-row-button"
						src={ChevronRight}
						alt="chevron-right"
					/>
				</div>
			</Link>
		);
	}
	var renderedItems;
	if (items.length > 0) {
		renderedItems = items.map((item) => {
			switch (type) {
				case "projects":
					return createLink(item.project, item.id, ROUTE.PROJECT);
				case "hours":
					const startTime = new Date(item.start_datetime);
					const day = startTime.getDate();
					const month = startTime.toLocaleString("nl-nl", {
						month: "short",
					});

					return createLink(
						item.project_name,
						item.id,
						ROUTE.WORKED_HOUR,
						month + "\n" + day,
						"added-date"
					);
				default:
					return <div key={item.id}></div>;
			}
		});
	} else {
		renderedItems = <div>Geen projecten gevonden.</div>;
	}

	return <div className="item-list">{renderedItems}</div>;
};

export default ItemList;
