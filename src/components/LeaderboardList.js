import "./LeaderboardList.css";

import React from "react";

const LeaderboardList = ({ leaderboard, renderPlaceHistory }) => {
	const leaderboardList = leaderboard.map((leaderboardItem) => {
		const currentUserClass = leaderboardItem.current_user
			? "current-user"
			: "";
		return (
			<div className="leaderboard-row" key={leaderboardItem.id}>
				<p
					className={currentUserClass}
				>{`#${leaderboardItem.placement}`}</p>
				<div className="leaderboard-item">
					<img
						className="avatar small"
						src={leaderboardItem.user.avatar}
						alt="user-avatar"
					/>
					<p className={currentUserClass}>
						{leaderboardItem.user.displayname}
					</p>
				</div>
				<img
					className="placement-history"
					src={renderPlaceHistory(leaderboardItem.placement_history)}
					alt="placement-history-icon"
				/>
			</div>
		);
	});

	return <div className="leaderboard-list">{leaderboardList}</div>;
};

export default LeaderboardList;
