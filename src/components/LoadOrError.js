import "./LoadOrError.css";

import React from "react";

import NotFound from "./NotFound";

const LoadOrError = ({ error }) => {
	if (error) {
		console.log(error.error);
		return <NotFound message={error.toString()} inAppRoute />;
	}
	return (
		<div className="content-height content-container loading-container">
			<div className="loading"></div>
		</div>
	);
};

export default LoadOrError;
