import React from "react";

import LogoBlue from "../assets/logo/logo_large_vertical.png";
import LogoWhite from "../assets/logo/logo_white_large.png";

const Logo = (props) => {
	return (
		<>
			<img
				className={props.classes}
				src={props.white ? LogoWhite : LogoBlue}
				alt="logo"
			/>
		</>
	);
};

export default Logo;
