import "./NotFound.css";

import React from "react";
import history from "../history";

import Button from "../components/Button";
import robot from "../assets/smiley/robot.png";

const NotFound = ({ inAppRoute, message = "404 - Niet gevonden!" }) => {
	const goBack = () => {
		history.goBack();
	};

	const inAppClass = inAppRoute ? "content-height" : "not-found-height";

	return (
		<div className="body-con">
			<div
				className={`content-container scroll-y not-found-con ${inAppClass} `}
			>
				<div>
					<img src={robot} alt="robot" />
					<h1>{message}</h1>
				</div>
				<Button text="Ga Terug" onClick={goBack} />
			</div>
		</div>
	);
};

export default NotFound;
