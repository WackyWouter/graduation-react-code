import React from "react";
import { connect } from "react-redux";
import { Route, Redirect } from "react-router-dom";

import { ROUTE } from "../constants/routeConstants";
import { signInWithKey } from "../actions";

const ProtectedRoute = (props) => {
	const { component: Component, ...rest } = props;
	// check if logged in
	const loggedIn = () => {
		const isAuthenticated = localStorage.getItem("isAuthenticated");

		if (isAuthenticated) {
			props.signInWithKey();
			return true;
		}
		return false;
	};

	const renderCom = (props) => {
		if (loggedIn()) {
			return <Component {...props} />;
		}

		// if not redirect to login
		return <Redirect to={ROUTE.LOGIN} />;
	};

	return <Route {...rest} render={renderCom} />;
};

const mapStateToProps = (state) => {
	return { isSignedIn: state.user.isSignedIn };
};

export default connect(mapStateToProps, { signInWithKey })(ProtectedRoute);
