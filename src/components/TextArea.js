import React from "react";

const TextArea = ({ input, label, meta: { touched, error } }) => {
	const errorClass = touched && error ? "input-error" : "";
	return (
		<div className="input-container">
			<textarea className={errorClass} {...input}>
				{label}
			</textarea>
		</div>
	);
};

export default TextArea;
