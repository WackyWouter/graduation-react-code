export const GET_ACTIVITIES = "GET_ACTIVITIES";
export const GET_HOURS = "GET_WORKED_HOURS";
export const GET_PROJECTS = "GET_PROJECTS";
export const GET_LEADERBOARD = "GET_LEADERBOARD";
export const GET_FEEDBACK = "GET_FEEDBACK";
export const GET_USER_INFO = "GET_USER_INFO";
export const POST_HOURS = "REGISTER_WORKED_HOURS";
export const FEEDBACK_SEEN = "FEEDBACK_SEEN";
export const SORT_PROJECTS = "SORT_PROJECTS";
export const SET_SEARCH = "SET_PROJECT_SEARCH";
export const CLEAR_SEARCH = "CLEAR_SEARCH";
export const SET_ERROR = "SET_CONTENT_ERROR";
export const CLEAR_ERROR = "CLEAR_CONTENT_ERROR";
export const STANDERD_ERROR = "Iets is fout gegaan. Probeer het later nog eens";

export const APP_ID = 1;
export const DIVISION_ID = 1;
