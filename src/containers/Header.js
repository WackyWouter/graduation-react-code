import "./Header.css";

import React, { Component } from "react";
import { connect } from "react-redux";

import StreakIcon from "../assets/streak-icon.svg";
import MenuIcon from "../assets/menu-icon.svg";
import CloseMenuIcon from "../assets/close-menu-icon.svg";
import { openMenu, closeMenu } from "../actions";

class Header extends Component {
	toggleMenu = () => {
		if (this.props.menuIsOpen) {
			this.props.closeMenu();
		} else {
			this.props.openMenu();
		}
	};

	render() {
		return (
			<div className="header">
				<div className="streak-container">
					<span>{this.props.streak}</span>
					<img src={StreakIcon} alt="Streak-icon" />
				</div>
				<p>{this.props.pageTitle}</p>
				<img
					src={this.props.menuIsOpen ? CloseMenuIcon : MenuIcon}
					onClick={this.toggleMenu}
					alt={
						this.props.menuIsOpen ? "close-menu-icon" : "menu-icon"
					}
				/>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return { menuIsOpen: state.display.menuIsOpen };
};

export default connect(mapStateToProps, { openMenu, closeMenu })(Header);
