import "./Menu.css";

import React, { Component } from "react";
import { connect } from "react-redux";
import history from "../history";

import Logo from "../components/Logo";
import IconButton from "../components/IconButton";
import HomeIcon from "../assets/home-icon.svg";
import LeaderboardIcon from "../assets/leaderboard-icon.svg";
import ProfileIcon from "../assets/profile-icon.svg";
import WorkedHoursIcon from "../assets/worked-hours-icon.svg";
import ProjectsIcon from "../assets/projects-icon.svg";
import FeedbackIcon from "../assets/feedback-icon.svg";
import LogoutIcon from "../assets/logout-icon.svg";
import { closeMenu, signOut, getFeedback } from "../actions";
import { ROUTE } from "../constants/routeConstants";
import InfoGreen from "../assets/info-green.svg";

class Menu extends Component {
	logout = () => {
		this.props.closeMenu();
		this.props.signOut();
		history.push(ROUTE.LOGIN);
	};

	closeMenu = () => {
		this.props.closeMenu();
	};

	componentDidMount() {
		if (!this.props.feedback) {
			this.props.getFeedback(
				this.props.userKey ?? localStorage.getItem("key")
			);
		}
	}

	render() {
		const $classes = this.props.menuIsOpen ? "shown" : "hidden";

		return (
			<div className={`menu-container ${$classes}`}>
				<div className="menu-content">
					<Logo white classes="main-logo white" />
					<div className="links-container">
						<IconButton
							classes={$classes}
							icon={HomeIcon}
							text="Home"
							to={ROUTE.HOME}
							onClick={this.closeMenu}
						/>
						<IconButton
							classes={$classes}
							to={ROUTE.LEADERBOARD}
							icon={LeaderboardIcon}
							text="Leaderboard"
							onClick={this.closeMenu}
						/>
						<IconButton
							classes={$classes}
							to={ROUTE.PROFILE}
							icon={ProfileIcon}
							text="Profiel"
							onClick={this.closeMenu}
						/>
						<IconButton
							classes={$classes}
							to={ROUTE.WORKED_HOURS + "0"}
							icon={WorkedHoursIcon}
							text="Gewerkte Uren"
							onClick={this.closeMenu}
						/>
						<IconButton
							classes={$classes}
							to={ROUTE.PROJECTS}
							icon={ProjectsIcon}
							text="Projecten"
							onClick={this.closeMenu}
						/>
						<IconButton
							classes={$classes}
							to={ROUTE.FEEDBACK}
							icon={FeedbackIcon}
							text="Feedback"
							notSeen={
								this.props.feedback
									? this.props.feedback.notSeen
									: false
							}
							onClick={this.closeMenu}
						/>
						<IconButton
							classes={$classes}
							to={ROUTE.EXPLANATION}
							icon={InfoGreen}
							text="Help"
							onClick={this.closeMenu}
						/>
						<IconButton
							classes={$classes}
							icon={LogoutIcon}
							onClick={this.logout}
							text="Uitloggen"
						/>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		menuIsOpen: state.display.menuIsOpen,
		feedback: state.content.feedback,
		userKey: state.user.key,
	};
};

export default connect(mapStateToProps, { closeMenu, signOut, getFeedback })(
	Menu
);
