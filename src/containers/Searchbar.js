import "./Searchbar.css";

import React, { Component } from "react";

import Search from "../assets/search.svg";

class Searchbar extends Component {
	state = { term: "" };

	onFormSubmit = (event) => {
		event.preventDefault();

		console.log(this.props);

		this.props.onSearchSumbit(this.state.term);
	};

	render() {
		return (
			<form className="search-container" onSubmit={this.onFormSubmit}>
				<input
					className="search"
					value={this.state.term}
					onChange={(e) => this.setState({ term: e.target.value })}
					placeholder="Zoeken"
				/>
				<button>
					<img src={Search} alt="search-icon" />
				</button>
			</form>
		);
	}
}

export default Searchbar;
