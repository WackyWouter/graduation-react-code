export function formatDate(input) {
	var date = new Date(input);
	return [
		date.getFullYear(),
		("0" + (date.getMonth() + 1)).slice(-2),
		("0" + date.getDate()).slice(-2),
	].join("-");
}

export function formatTime(input) {
	var date = new Date(input);
	return [
		("0" + date.getHours()).slice(-2),
		("0" + date.getMinutes()).slice(-2),
	].join(":");
}

export function compareStartEndTime(start, end) {
	start = start.split(":");
	end = end.split(":");
	const start_hours = parseInt(start[0]);
	const start_min = parseInt(start[1]);
	const end_hours = parseInt(end[0]);
	const end_min = parseInt(end[1]);

	if (start_hours >= end_hours) {
		if (start_min < end_min && start_hours === end_hours) {
			return true;
		}
		return false;
	}
	return true;
}
