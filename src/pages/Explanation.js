import "./Explanation.css";

import React, { Component } from "react";
import { connect } from "react-redux";

import { setPageTitle } from "../actions";

class Explanation extends Component {
	componentDidMount() {
		if (this.props.pageTitle !== "Help") {
			this.props.setPageTitle("Help");
		}
	}
	render() {
		return (
			<div className="content-container scroll-y content-height help-list-con">
				<div>
					<p className="title mb-5">Home</p>
					<p className="help no-margin">
						Op deze pagina kunt u uw uren registeren. Op deze pagina
						zult uw ook een suggestie krijgen voor uw gewerkte uren
						op basis van uw locatie.
					</p>
				</div>
				<div>
					<p className="title  mb-5">Leaderboard</p>
					<p className="help no-margin">
						Hier kan je bijhouden wat u score is ten opzichten van
						uw collega's. Een rode driehoek naar beneden geeft aan
						dat sinds vorige week de persoon is gestegen op het
						leaderboard. Een groene driehoek naar boven geeft aan
						dat de persoon gezakt is op het leaderboard sinds vorige
						week. Een zwarte streep geeft aan dat de persoon op
						dezelfde plek is gebleven sinds vorige week.
					</p>
				</div>
				<div>
					<p className="title  mb-5">Streak</p>
					<p className="help no-margin">
						De streak houd bij hoeveel dagen u uw uren registreert
						op de dag uw ze gemaakt heeft. Wanneer u uren
						registreert op een andere dag dan dat u ze gemaakt heeft
						zal uw streak verloren gaan. Uw streak zal ook alleen
						omhoog gaan wanneer uw uren invult op de gemaakte dag.
					</p>
				</div>
				<div>
					<p className="title  mb-5">Profiel</p>
					<p className="help no-margin">
						Hier kunt u uw profiel foto, naam, wachtwoord en email
						aanpassen. Verder kunt u hier ook statistieken zien over
						uw streak, leaderboard en feedback
					</p>
				</div>
				<div>
					<p className="title  mb-5">Gewerkte Uren</p>
					<p className="help no-margin">
						Hier kunt u uw gewerkte uren terug zien. Het is ook
						mogelijk om de gewerkte uren aan te passen zolang als
						dat ze niet definitief gemaakt zijn door de
						administratieve medewerkers.
					</p>
				</div>
				<div>
					<p className="title  mb-5">Projecten</p>
					<p className="help no-margin">
						Hier kunt u de huidige projecten doorzoeken en bekijken.
						Het is ook mogelijk om via een project alleen de
						gewerkte uren voor dat project te zien.
					</p>
				</div>
				<div>
					<p className="title  mb-5">Feedback</p>
					<p className="help no-margin">
						Hier kunt u uw feedback bekijken. Het is ook mogelijk om
						nieuwe feedback op te vragen. Wanneer nieuwe feedback
						beschikbaar is en uw heeft deze feedback nog niet
						bekeken dan zal dit aangegeven worden met een rood
						uitroepteken in het menu.
					</p>
				</div>
			</div>
		);
	}
}

export default connect(null, { setPageTitle })(Explanation);
