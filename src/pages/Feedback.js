import "./Feedback.css";

import React, { Component } from "react";
import { connect } from "react-redux";

import {
	getFeedback,
	setPageTitle,
	setFeedbackSeen,
	askForMoreFeedback,
} from "../actions";
import FeedbackBox from "../components/FeedbackBox";
import Button from "../components/Button";

class Feedback extends Component {
	componentDidMount() {
		if (this.props.pageTitle !== "Feedback") {
			this.props.setPageTitle("Feedback");
		}

		if (!this.props.feedback) {
			this.props.getFeedback(this.props.userKey);
		}
	}

	setFeedbackSeen(feedback) {
		if (feedback.notSeen) {
			// Set feedback to seen
			this.props.setFeedbackSeen(this.props.userKey, feedback);
		}
	}

	renderFeedback(feedback) {
		const notFound =
			"Er kon geen feedback gevonden worden voor u op dit moment. Probeer het later nog eens.";
		if (feedback) {
			this.setFeedbackSeen(feedback);

			return (
				<FeedbackBox
					score={feedback.score}
					feedback={feedback.feedback}
				/>
			);
		}
		return <FeedbackBox score={0} feedback={notFound} />;
	}

	onClick = () => {
		// dispatch action for more feedback
		this.props.askForMoreFeedback(this.props.userKey);
	};

	render() {
		return (
			<div className="content-container content-height scroll-y feedback-container">
				{this.renderFeedback(this.props.feedback)}
				<Button text="Vraag om meer Feedback" onClick={this.onClick} />
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		userKey: state.user.key,
		feedback: state.content.feedback,
	};
};

export default connect(mapStateToProps, {
	getFeedback,
	setPageTitle,
	setFeedbackSeen,
	askForMoreFeedback,
})(Feedback);
