import "./ForgotPassword.css";

import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";

import { forgotPassword, clear, signOut } from "../actions";
import Logo from "../components/Logo";
import Input from "../components/Input";
import Button from "../components/Button";
import ErrorMessage from "../components/ErrorMessage";
import Back from "../components/Back";

class ForgotPassword extends Component {
	onSubmit = (formValues) => {
		this.props.forgotPassword(formValues);
	};

	componentDidMount() {
		this.props.signOut();
	}

	render() {
		return (
			<div className="forgot-page-container scroll-y">
				<Back />
				<div className="forgot-container">
					<Logo classes="main-logo" />
					<div className="form-container">
						<div>
							<p className="title no-margin">
								Wachtwoord Vergeten
							</p>
							<p className="description">
								Vul uw email in en wij zullen u een email sturen
								met een link om uw wachtwoord aan te passen.
							</p>
						</div>
						<ErrorMessage error={this.props.onSubmitError} />
						<form
							onSubmit={this.props.handleSubmit(this.onSubmit)}
							className="login-form"
						>
							<Field
								name="email"
								component={Input}
								type="email"
								label="Email"
							/>
							<Button text="Stuur Email" />
						</form>
					</div>
				</div>
			</div>
		);
	}
	componentWillUnmount() {
		// clear error log before moving on to next page
		this.props.clear();
	}
}

const validate = (formValues) => {
	const errors = {};

	if (!formValues.email) {
		// only ran if the user did not enter a title
		errors.email = true;
	}

	return errors;
};

const form = reduxForm({
	form: "forgottenPasswordForm",
	validate: validate,
})(ForgotPassword);

const mapStateToProps = (state) => {
	return { onSubmitError: state.user.error };
};

export default connect(mapStateToProps, { forgotPassword, clear, signOut })(
	form
);
