import "./Home.css";

import React, { Component } from "react";
import { connect } from "react-redux";

import AlertBox from "../components/AlertBox";
import Button from "../components/Button";
import { ROUTE } from "../constants/routeConstants";
import {
	closeSug,
	getProjects,
	getHours,
	setPageTitle,
	setCoords,
	setSugDetails,
	clearSug,
	acceptSug,
	clearError,
} from "../actions";
import ItemList from "../components/ItemList";
import history from "../history";
import LoadOrError from "../components/LoadOrError";

class Home extends Component {
	constructor(props) {
		super(props);
		this.myRef = React.createRef();
	}

	onNoClick = () => {
		this.props.closeSug();
		this.props.clearSug();
	};

	onYesClick = () => {
		this.props.acceptSug();
	};

	onRegHoursClick = () => {
		this.props.clearSug();
		history.push(ROUTE.REG_HOURS);
	};

	componentDidMount() {
		if (this.props.pageTitle !== "Home") {
			this.props.setPageTitle("Home");
		}

		const key = this.props.userKey ?? localStorage.getItem("key");

		// Get needed content if it doesnt exist in the redux store
		if (this.props.projects.length === 0) {
			this.props.getProjects(key);
		}
		if (this.props.hours.length === 0) {
			this.props.getHours(key);
		}

		// ask for location permission
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition((position) => {
				this.props.setCoords(position.coords);
			});
		} else {
			alert(
				"Zet locatie aan om gebruik te maken van suggestie functionaliteit."
			);
		}
	}

	render() {
		if (this.props.projects.length > 0 && this.props.hours.length > 0) {
			if (
				!this.props.sugGotDenied &&
				!this.props.sugGotAccepted &&
				!this.props.sugText
			) {
				this.props.setSugDetails(
					this.props.projects,
					this.props.coords
				);
			}

			// Remove alert box from the flex box parent after animation finished
			const alertbox = this.myRef.current;
			if (
				(this.props.sugGotDenied || this.props.sugGotAccepted) &&
				alertbox
			) {
				setTimeout(function () {
					alertbox.classList.add("display-none");
				}, 500);
			}

			const classes = this.props.sugIsOpen ? "shown" : "hidden";
			return (
				<>
					<div className="content-container gap-30 scroll-y content-height">
						<AlertBox
							ref={this.myRef}
							text={this.props.sugText}
							btnNum="two"
							classes={classes}
						>
							<Button
								classes="small inline no-margin"
								text="Ja"
								onClick={this.onYesClick}
							/>
							<Button
								classes="inverted small inline no-margin"
								text="Nee"
								onClick={this.onNoClick}
							/>
						</AlertBox>
						<Button
							onClick={this.onRegHoursClick}
							text="Registreer Uren"
						/>
						<div>
							<p className="title mb-5">Recent Gewerkte Uren</p>
							<ItemList items={this.props.hours} type="hours" />
						</div>
						<Button
							to={ROUTE.WORKED_HOURS + "0"}
							text="Meer Recent Gewerkte Uren"
						/>
					</div>
				</>
			);
		} else {
			return <LoadOrError error={this.props.contentError} />;
		}
	}
	componentWillUnmount() {
		this.props.clearError();
	}
}

const mapStateToProps = (state) => {
	var hours = state.content.hours;

	// Add project name to worked hours
	if (state.content.projects.length > 0) {
		hours.forEach((hour) => {
			const project = state.content.projects.find(
				(project) => project.id === hour.project_id
			);

			hour.project_name = project.project ?? "Project niet gevonden";
		});
	}

	if (state.display.sugGotDenied) {
		hours = hours.slice(0, 8);
	} else {
		hours = hours.slice(0, 3);
	}

	return {
		sugIsOpen: state.display.sugIsOpen,
		userKey: state.user.key,
		hours: hours,
		activities: state.content.activities,
		projects: state.content.projects,
		sugText: state.display.sugText,
		coords: state.user.coords,
		sugGotDenied: state.display.sugGotDenied,
		sugGotAccepted: state.display.sugGotAccepted,
		contentError: state.content.error,
	};
};

export default connect(mapStateToProps, {
	closeSug,
	getProjects,
	getHours,
	setPageTitle,
	setSugDetails,
	setCoords,
	clearSug,
	acceptSug,
	clearError,
})(Home);
