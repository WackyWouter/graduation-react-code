import "./Leaderboard.css";

import React, { Component } from "react";
import { connect } from "react-redux";

import { getLeaderboard, setPageTitle, clearError } from "../actions";
import Crown from "../assets/crown.svg";
import Up from "../assets/up.svg";
import Down from "../assets/down.svg";
import Steady from "../assets/steady.svg";
import Dots from "../assets/three-dots.svg";
import LeaderboardList from "../components/LeaderboardList";
import LoadOrError from "../components/LoadOrError";

class Leaderboard extends Component {
	componentDidMount() {
		if (this.props.pageTitle !== "Leaderboard") {
			this.props.setPageTitle("Leaderboard");
		}

		if (this.props.leaderboard.length === 0) {
			this.props.getLeaderboard(
				this.props.userKey ?? localStorage.getItem("key")
			);
		}
	}

	renderPlaceHistory(history) {
		switch (history) {
			case "up":
				return Up;
			case "down":
				return Down;
			case "steady":
			default:
				return Steady;
		}
	}

	renderLeaderboard() {
		const { leaderboard } = this.props;
		// Get placement of current user plus 1 to start countinf from 1 not 0
		const userIndex =
			leaderboard.findIndex((element) => element.current_user) + 1;

		// Return whole leaderboard its short anyway
		if (leaderboard.length <= 10 && userIndex <= 10) {
			return (
				<LeaderboardList
					leaderboard={leaderboard.slice(1, leaderboard.length)}
					renderPlaceHistory={this.renderPlaceHistory}
				/>
			);
			// Return the first ten since the user is wihting that
		} else if (leaderboard.length > 10 && userIndex < 10) {
			return (
				<LeaderboardList
					leaderboard={leaderboard.slice(1, 10)}
					renderPlaceHistory={this.renderPlaceHistory}
				/>
			);
		} else {
			const userPlace = () => {
				// Check if user is in last place
				if (userIndex !== leaderboard.length) {
					return (
						// return the place behind the user, the user and the two in front of the user
						<LeaderboardList
							leaderboard={leaderboard.slice(
								userIndex - 3,
								userIndex + 1
							)}
							renderPlaceHistory={this.renderPlaceHistory}
						/>
					);
				} else {
					return (
						// return  the user and the trhee in front of the user
						<LeaderboardList
							leaderboard={leaderboard.slice(
								userIndex - 4,
								userIndex
							)}
							renderPlaceHistory={this.renderPlaceHistory}
						/>
					);
				}
			};

			// Return the first 5 and show the 4 around the current user divided by three dots
			return (
				<div className="dots-divided-list">
					<LeaderboardList
						leaderboard={leaderboard.slice(1, 5)}
						renderPlaceHistory={this.renderPlaceHistory}
					/>
					<img src={Dots} alt="three-dots-icon" />
					{userPlace()}
				</div>
			);
		}
	}

	render() {
		if (this.props.leaderboard.length > 0) {
			const { leaderboard } = this.props;
			return (
				<div className="content-container content-height scroll-y">
					<div className="leaderboard-container">
						<div className="leaderboard-leader">
							<img
								src={leaderboard[0].user.avatar}
								className="avatar"
								alt="avatar-user"
							/>
							<img
								src={Crown}
								className="crown-icon"
								alt="crown-icon"
							/>
						</div>
						<div className="leader-name-container">
							<p className="displayname leader-name">
								{leaderboard[0].user.displayname}
							</p>
							<img
								src={this.renderPlaceHistory(
									leaderboard[0].placement_history
								)}
								className="placement-history"
								alt="placement-history-icon"
							/>
						</div>
						{this.renderLeaderboard()}
					</div>
				</div>
			);
		}
		return <LoadOrError error={this.props.error} />;
	}

	componentWillUnmount() {
		this.props.clearError();
	}
}

const mapStateToProps = (state) => {
	return {
		userKey: state.user.key,
		leaderboard: state.content.leaderboard,
		contentError: state.content.error,
	};
};

export default connect(mapStateToProps, {
	getLeaderboard,
	setPageTitle,
	clearError,
})(Leaderboard);
