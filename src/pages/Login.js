import "./Login.css";

import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import { signIn, clear, signInWithKey } from "../actions";
import { Link } from "react-router-dom";

import Logo from "../components/Logo";
import Input from "../components/Input";
import Button from "../components/Button";
import ErrorMessage from "../components/ErrorMessage";
import history from "../history";
import { ROUTE } from "../constants/routeConstants";

class Login extends Component {
	onSubmit = (formValues) => {
		this.props.signIn(formValues);
	};

	componentDidMount() {
		const isAuthenticated = localStorage.getItem("isAuthenticated");

		if (isAuthenticated) {
			this.props.signInWithKey();
			history.push(ROUTE.HOME);
		}
	}

	render() {
		return (
			<div className="login-container">
				<Logo classes="main-logo" />
				<div className="form-container">
					<p className="title no-margin">Log In</p>
					<ErrorMessage error={this.props.onSubmitError} />
					<form
						onSubmit={this.props.handleSubmit(this.onSubmit)}
						className="login-form"
					>
						<Field
							name="email"
							component={Input}
							type="email"
							label="Email"
						/>
						<Field
							name="password"
							component={Input}
							type="password"
							label="Wachtwoord"
						/>
						<Field
							name="remember"
							component={Input}
							type="checkbox"
							label="Onthoud details"
						/>

						<Button text="Log In" classes="mt-10" />
					</form>

					<div className="green-link-con">
						<Link className="green-link" to={ROUTE.FORGOT_PW}>
							Wachtwoord Vergeten
						</Link>
					</div>
				</div>
			</div>
		);
	}
	componentWillUnmount() {
		// clear error log before moving on to next page
		this.props.clear();
	}
}

const validate = (formValues) => {
	const errors = {};

	if (!formValues.email) {
		// only ran if the user did not enter a email
		errors.email = true;
	}

	if (!formValues.password) {
		// only ran if the user did not enter a password
		errors.password = true;
	}
	return errors;
};

const form = reduxForm({
	form: "loginForm",
	validate: validate,
	enableReinitialize: true,
})(Login);

const mapStateToProps = (state) => {
	var initialValues = {};

	const email = localStorage.getItem("email");

	if (email) {
		initialValues.email = email;
		initialValues.remember = true;
	}

	return {
		onSubmitError: state.user.error,
		isSignedIn: state.user.isSignedIn,
		initialValues: initialValues,
	};
};

export default connect(mapStateToProps, { signIn, clear, signInWithKey })(form);
