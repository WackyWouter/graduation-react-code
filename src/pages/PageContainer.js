import React, { Component } from "react";
import { connect } from "react-redux";

import ProtectedRoute from "../components/ProtectedRoute";
import Header from "../containers/Header";
import Menu from "../containers/Menu";
import Leaderboard from "./Leaderboard";
import Home from "./Home";
import { ROUTE } from "../constants/routeConstants";
import { Switch } from "react-router";
import NotFound from "../components/NotFound";
import Projects from "./Projects";
import WorkedHours from "./WorkedHours";
import Feedback from "./Feedback";
import Profile from "./Profile";
import RegisterHours from "./RegisterHours";
import ProjectDetail from "./ProjectDetail";
import WorkedHoursDetail from "./WorkedHoursDetail";
import Explanation from "./Explanation";

class PageContainer extends Component {
	render() {
		return (
			<>
				<Header
					pageTitle={this.props.pageTitle}
					streak={this.props.streak}
				/>
				<Menu />
				<Switch>
					<ProtectedRoute path={ROUTE.HOME} exact component={Home} />
					<ProtectedRoute
						path={ROUTE.LEADERBOARD}
						exact
						component={Leaderboard}
					/>
					<ProtectedRoute
						path={ROUTE.PROJECTS}
						exact
						component={Projects}
					/>
					<ProtectedRoute
						path={ROUTE.WORKED_HOURS + ":conf/:project?"}
						exact
						component={WorkedHours}
					/>
					<ProtectedRoute
						path={ROUTE.FEEDBACK}
						exact
						component={Feedback}
					/>
					<ProtectedRoute
						path={ROUTE.PROFILE}
						exact
						component={Profile}
					/>
					<ProtectedRoute
						path={ROUTE.REG_HOURS}
						exact
						component={RegisterHours}
					/>
					<ProtectedRoute
						path={ROUTE.PROJECT + ":id"}
						exact
						component={ProjectDetail}
					/>
					<ProtectedRoute
						path={ROUTE.WORKED_HOUR + ":id"}
						exact
						component={WorkedHoursDetail}
					/>
					<ProtectedRoute
						path={ROUTE.EDIT_HOURS + ":id"}
						exact
						component={RegisterHours}
					/>
					<ProtectedRoute
						path={ROUTE.EXPLANATION}
						exact
						component={Explanation}
					/>
					<ProtectedRoute path="*" inAppRoute component={NotFound} />
				</Switch>
			</>
		);
	}
}

const mapStateToProps = (state) => {
	return { pageTitle: state.display.pageTitle, streak: state.user.streak };
};

export default connect(mapStateToProps)(PageContainer);
