import "./Profile.css";

import React, { Component } from "react";
import { connect } from "react-redux";
import { Field, reduxForm } from "redux-form";

import { setPageTitle, getUserInfo, clearError } from "../actions";
import Button from "../components/Button";
import { ROUTE } from "../constants/routeConstants";
import Picture from "../assets/picture.svg";
import Input from "../components/Input";
import LoadOrError from "../components/LoadOrError";

class Profile extends Component {
	componentDidMount() {
		if (this.props.pageTitle !== "Profiel") {
			this.props.setPageTitle("Profiel");
		}
		console.log(this.props.userKey);

		if (!this.props.userInfo) {
			this.props.getUserInfo(
				this.props.userKey ?? localStorage.getItem("key")
			);
		}
	}

	onSubmit = (formValues) => {
		// TODO do action to change name and email on submit
	};

	render() {
		if (this.props.userInfo) {
			const { userInfo } = this.props;
			return (
				<form
					className="content-height content-container scroll-y profile-container"
					onSubmit={this.props.handleSubmit(this.onSubmit)}
				>
					<div className="userinfo-container">
						<div className="column-center">
							<div className="avatar-container">
								<img
									className="avatar"
									src={userInfo.avatar}
									alt="profiel foto"
								/>
								<img
									className="picture-icon shadow"
									alt="camera-icon"
									src={Picture}
								/>
							</div>
							<p className="displayname">
								{userInfo.displayname}
							</p>
						</div>
						<div>
							<p className="title mb-5">Naam</p>
							<Field
								name="displayname"
								component={Input}
								type="text"
							/>
						</div>
						<div>
							<p className="title mb-5">Email</p>
							<Field name="email" component={Input} type="text" />
						</div>
						<div>
							<p className="title mb-5">Statistieken</p>
							<div className="stat-row mb-5">
								<p className="fs-18 no-margin">
									Hoogste streak
								</p>
								<p className="fs-18 no-margin">
									{userInfo.highest_streak}
								</p>
							</div>
							<div className="stat-row mb-5">
								<p className="fs-18 no-margin">
									Hoogste plek leaderboard{" "}
								</p>
								<p className="fs-18 no-margin">
									{`#${userInfo.highest_place}`}
								</p>
							</div>
							<div className="stat-row mb-5">
								<p className="fs-18 no-margin">
									Hoogste feedback score
								</p>
								<p className="fs-18 no-margin">
									{`${userInfo.highest_feedback}/5`}
								</p>
							</div>
						</div>
					</div>
					<div className="button-con">
						<Button text="Update Account" />
						<Button
							text="Verander Wachtwoord"
							to={ROUTE.FORGOT_PW}
						/>
					</div>
				</form>
			);
		} else {
			return <LoadOrError error={this.props.contentError} />;
		}
	}
	componentWillUnmount() {
		this.props.clearError();
	}
}

// Enable reinnitialize true needed to update form if initial values changes
const form = reduxForm({
	form: "profileForm",
	enableReinitialize: true,
})(Profile);

const mapStateToProps = (state) => {
	var initialValues = {};

	// Fill form with initial values
	if (state.content.userInfo) {
		initialValues.displayname = state.content.userInfo.displayname;
		initialValues.email = state.content.userInfo.email;
	}

	return {
		userInfo: state.content.userInfo,
		userKey: state.user.key,
		initialValues: initialValues,
		contentError: state.content.error,
	};
};

export default connect(mapStateToProps, {
	setPageTitle,
	getUserInfo,
	clearError,
})(form);
