import "./ProjectDetail.css";

import React, { Component } from "react";
import { connect } from "react-redux";

import Info from "../assets/info.svg";
import House from "../assets/house.svg";
import Phone from "../assets/phone.svg";
import Mail from "../assets/mail.svg";
import { GOOGLE_API_KEY } from "../constants/contentConstants";
import Button from "../components/Button";
import { getProjects, clearError } from "../actions";
import { ROUTE } from "../constants/routeConstants";
import Back from "../components/Back";
import LoadOrError from "../components/LoadOrError";

class ProjectDetail extends Component {
	componentDidMount() {
		if (this.props.projects.length === 0) {
			this.props.getProjects(
				this.props.userKey ?? localStorage.getItem("key")
			);
		}
	}

	renderMap() {
		if (this.props.project.address) {
			// Replace space withe +
			const address = this.props.project.address.replace(/\s/g, "+");
			const city = this.props.project.city.replace(/\s/g, "+");

			return (
				<iframe
					title="project-map"
					className="project-map"
					frameBorder="0"
					src={`https://www.google.com/maps/embed/v1/place?key=${GOOGLE_API_KEY}&q=${address},${city}`}
					allowFullScreen
				/>
			);
		}
		return null;
	}

	render() {
		//  the button at the bottom goes to all the hours you worked for this project
		const { project } = this.props;
		if (project) {
			return (
				<div className="content-height scroll-y content-container project-container ">
					<div className="back-image-con">
						<div className="back-con">
							<Back />
						</div>
						<div className="image-con">
							<img
								src={project.img}
								alt={`${project.project} logo`}
							/>
						</div>
					</div>

					<div className="project-row">
						<p className="title mb-5">Beschrijving</p>
						<div className="project-sub-row">
							<div>
								<img src={Info} alt="info-icon" />
							</div>
							<p className="project-description">
								{project.description}
							</p>
						</div>
					</div>
					<div className="project-row">
						<p className="title mb-5">adres</p>
						<div className="project-sub-row">
							<div>
								<img src={House} alt="house-icon" />
							</div>
							<p>
								{project.address} <br />
								{project.postal} <br />
								{project.city}
							</p>
						</div>
					</div>
					<div className="project-row">
						<p className="title mb-5">Contact</p>
						<div className="project-sub-row">
							<div>
								<img src={Phone} alt="phone-icon" />
							</div>
							<p>{project.phone}</p>
						</div>
						<div className="project-sub-row">
							<div>
								<img src={Mail} alt="mail-icon" />
							</div>
							<p>{project.email}</p>
						</div>
					</div>

					{this.renderMap()}

					<Button
						text="Gewerkte Uren"
						to={ROUTE.WORKED_HOURS + "0/" + project.id}
					/>
				</div>
			);
		}
		return <LoadOrError error={this.props.contentError} />;
	}

	componentWillUnmount() {
		this.props.clearError();
	}
}

const mapStateToProps = (state, ownProps) => {
	const project = state.content.projects.find(
		(project) => project.id.toString() === ownProps.match.params.id
	);

	return {
		project: project,
		projects: state.content.projects,
		userKey: state.user.key,
		contentError: state.content.error,
	};
};

export default connect(mapStateToProps, { getProjects, clearError })(
	ProjectDetail
);
