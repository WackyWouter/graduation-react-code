import "./Projects.css";

import React, { Component } from "react";
import { connect } from "react-redux";

import {
	getProjects,
	setPageTitle,
	sortProjects,
	searchProjects,
	clearSearch,
	clearError,
} from "../actions";
import ItemList from "../components/ItemList";
import Searchbar from "../containers/Searchbar";
import Sort from "../assets/sort.svg";
import LoadOrError from "../components/LoadOrError";

class Projects extends Component {
	componentDidMount() {
		if (this.props.pageTitle !== "Projecten") {
			this.props.setPageTitle("Projecten");
		}

		if (this.props.projects.length === 0) {
			this.props.getProjects(
				this.props.userKey ?? localStorage.getItem("key")
			);
		}
	}

	onSearchSumbit = (term) => {
		if (term === "") {
			this.props.clearSearch();
		} else {
			this.props.searchProjects(term, this.props.projects);
		}
	};

	onSortClick = () => {
		this.props.sortProjects(this.props.filteredProjects, this.props.aToZ);
	};

	render() {
		if (this.props.projects.length > 0) {
			return (
				<div className="content-container gap-20 content-height scroll-y">
					<div className="filter-container">
						<Searchbar onSearchSumbit={this.onSearchSumbit} />
						<img
							src={Sort}
							alt="sort-icon"
							onClick={this.onSortClick}
						/>
					</div>
					<ItemList
						items={this.props.filteredProjects}
						type="projects"
					/>
				</div>
			);
		} else {
			return <LoadOrError error={this.props.contentError} />;
		}
	}

	componentWillUnmount() {
		this.props.clearSearch();
		this.props.clearError();
	}
}

const mapStateToProps = (state) => {
	return {
		userKey: state.user.key,
		filteredProjects: state.content.filteredProjects,
		projects: state.content.projects,
		pageTitle: state.display.pageTitle,
		aToZ: state.content.projectsAToZ,
		contentError: state.content.error,
	};
};

export default connect(mapStateToProps, {
	getProjects,
	setPageTitle,
	sortProjects,
	searchProjects,
	clearSearch,
	clearError,
})(Projects);
