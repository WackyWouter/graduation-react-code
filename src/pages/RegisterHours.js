import "./RegisterHours.css";

import React, { Component } from "react";
import { Field, reduxForm, formValueSelector } from "redux-form";
import { connect } from "react-redux";

import Button from "../components/Button";
import Input from "../components/Input";
import TextArea from "../components/TextArea";
import Dropdown from "../components/Dropdown";
import {
	getActivities,
	getProjects,
	getHours,
	postHours,
	clearSug,
} from "../actions";
import {
	compareStartEndTime,
	formatDate,
	formatTime,
} from "../helperFunctions/Date";

class RegisterHours extends Component {
	componentDidMount() {
		const key = this.props.userKey ?? localStorage.getItem("key");
		// Get needed content if it doesnt exist in the redux store
		if (this.props.projects.length === 0) {
			this.props.getProjects(key);
		}
		if (this.props.activities.length === 0) {
			this.props.getActivities(key);
		}
		if (this.props.hours.length === 0) {
			this.props.getHours(key);
		}
	}

	getProjectActivities() {
		const { projectSelect, projects, activities } = this.props;
		const options = projects.find(
			(project) => project.id === parseInt(projectSelect)
		);

		if (options) {
			return options.activities;
		} else {
			return activities;
		}
	}

	onSubmit = (formValues) => {
		if (this.props.selectedHour) {
			// update posted hours action
			this.props.postHours(
				this.props.hours,
				formValues,
				this.props.userKey,
				this.props.selectedHour.id
			);
			// clear suggestion
			this.props.clearSug();
		} else {
			// do post hours action
			this.props.postHours(
				this.props.hours,
				formValues,
				this.props.userKey
			);
		}
	};

	render() {
		return (
			<form
				className="content-container reg-hours-container content-height"
				onSubmit={this.props.handleSubmit(this.onSubmit)}
			>
				<div className="form-row">
					<p className="title ">Project</p>
					<Field
						name="project"
						component={Dropdown}
						options={this.props.projects}
						type="text"
						placeholder="project"
					/>
				</div>
				<div className="form-row">
					<p className="title ">Datum</p>
					<Field name="date" component={Input} type="date" />
				</div>
				<div className="form-row-time">
					<div className="form-column">
						<p className="title ">Start tijd</p>
						<Field
							name="startTime"
							component={Input}
							type="time"
							placeholder="Start tijd"
						/>
					</div>
					<div className="form-column">
						<p className="title ">Eind tijd</p>
						<Field
							name="endTime"
							component={Input}
							type="time"
							placeholder="Eind tijd"
						/>
					</div>
				</div>
				<div className="form-row">
					<p className="title ">Activiteit</p>
					<Field
						name="activity"
						component={Dropdown}
						options={this.getProjectActivities()}
						type="text"
						placeholder="Activiteit"
					/>
				</div>
				<div className="form-row">
					<p className="title ">Opmerkingen</p>
					<Field
						name="remarks"
						component={TextArea}
						type="text"
						placeholder="Opmerkingen"
					/>
				</div>

				<Button text="Registreer Uren" />
			</form>
		);
	}
}
const validate = (formValues) => {
	const errors = {};

	if (!formValues.project) {
		errors.project = true;
	}

	if (!formValues.date) {
		errors.date = true;
	}

	if (!formValues.startTime) {
		errors.startTime = true;
	}

	if (!formValues.endTime) {
		errors.endTime = true;
	}

	if (!formValues.activity) {
		errors.activity = true;
	}

	if (
		formValues.endTime &&
		formValues.startTime &&
		!compareStartEndTime(formValues.startTime, formValues.endTime)
	) {
		errors.startTime = true;
		errors.endTime = true;
	}

	return errors;
};

const form = reduxForm({
	form: "regHoursForm",
	validate: validate,
	enableReinitialize: true,
})(RegisterHours);

const mapStateToProps = (state, ownProps) => {
	// Retrieve selected hour if coming from workedhours detail page
	const hour = state.content.hours.find(
		(hour) => hour.id.toString() === ownProps.match.params.id
	);

	var initialValues = {};

	// Fill form with initial values
	if (hour) {
		// fill with values from worked hours detail page
		const start = new Date(hour.start_datetime);
		const end = new Date(hour.end_datetime);

		initialValues.project = hour.project_id;
		initialValues.date = formatDate(hour.start_datetime);
		initialValues.startTime = formatTime(start);
		initialValues.endTime = formatTime(end);
		initialValues.activity = hour.activity_id;
		initialValues.remarks = hour.description;
	} else if (state.display.sugGotAccepted && state.display.sugProjectId) {
		// fill with suggestion details
		const today = new Date();

		initialValues.project = state.display.sugProjectId;
		initialValues.activity = state.display.sugActivityId;
		initialValues.date = formatDate(today);
		initialValues.endTime = formatTime(today);
	}

	// Make project available in component without submitting the form
	const selector = formValueSelector("regHoursForm");

	return {
		projects: state.content.projects,
		activities: state.content.activities,
		hours: state.content.hours,
		selectedHour: hour,
		initialValues: initialValues,
		userKey: state.user.key,
		projectSelect: selector(state, "project"),
	};
};

export default connect(mapStateToProps, {
	getActivities,
	getProjects,
	getHours,
	postHours,
	clearSug,
})(form);
