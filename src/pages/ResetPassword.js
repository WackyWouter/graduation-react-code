import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import { resetPw, clear } from "../actions";

import Logo from "../components/Logo";
import Input from "../components/Input";
import Button from "../components/Button";
import ErrorMessage from "../components/ErrorMessage";

class ResetPassword extends Component {
	onSubmit = (formValues) => {
		this.props.resetPw(formValues);
	};

	render() {
		return (
			<div className="login-container reset-password">
				<Logo classes="main-logo" />
				<div className="form-container reset-password">
					<div>
						<p className="title no-margin">Reset Wachtwoord</p>
						<p className="description">
							Vul uw email en uw nieuw wachtwoord in.
						</p>
					</div>
					<ErrorMessage error={this.props.onSubmitError} />
					<form
						onSubmit={this.props.handleSubmit(this.onSubmit)}
						className="login-form reset-password"
					>
						<Field
							name="email"
							component={Input}
							type="email"
							label="Email"
						/>
						<Field
							name="password"
							component={Input}
							type="password"
							label="Wachtwoord"
						/>
						<Field
							name="confirmPassword"
							component={Input}
							type="password"
							label="Bevestig Wachtwoord"
						/>

						<Button text="Reset Wachtwoord" />
					</form>
				</div>
			</div>
		);
	}
	componentWillUnmount() {
		// clear error log before moving on to next page
		this.props.clear();
	}
}

const validate = (formValues) => {
	const errors = {};

	if (!formValues.email) {
		// only ran if the user did not enter a title
		errors.email = true;
	}

	if (!formValues.password) {
		// only ran if the user did not enter a title
		errors.password = true;
	}

	if (!formValues.confirmPassword) {
		// only ran if the user did not enter a title
		errors.confirmPassword = true;
	}

	return errors;
};

const form = reduxForm({
	form: "resetPasswordForm",
	validate: validate,
})(ResetPassword);

const mapStateToProps = (state) => {
	return { onSubmitError: state.user.error };
};

export default connect(mapStateToProps, { resetPw, clear })(form);
