import React, { Component } from "react";
import { connect } from "react-redux";

import { getHours, setPageTitle, getProjects, clearError } from "../actions";
import AlertBox from "../components/AlertBox";
import Button from "../components/Button";
import ItemList from "../components/ItemList";
import LoadOrError from "../components/LoadOrError";

class WorkedHours extends Component {
	constructor(props) {
		super(props);
		this.myRef = React.createRef();
	}
	componentDidMount() {
		const key = this.props.userKey ?? localStorage.getItem("key");
		if (this.props.pageTitle !== "Gewerkte Uren") {
			this.props.setPageTitle("Gewerkte Uren");
		}

		if (this.props.hours.length === 0) {
			this.props.getHours(key);
		}
		if (this.props.projects.length === 0) {
			this.props.getProjects(key);
		}
	}

	renderConfirmation() {
		if (parseInt(this.props.match.params.conf) === 1) {
			const alertboxRef = this.myRef.current;
			return (
				<AlertBox
					ref={this.myRef}
					text="Uw werk uren zijn succesvol geregistreerd."
					btnNum="one"
				>
					<Button
						classes="small inline no-margin"
						text="Okay"
						onClick={() => this.closeConfirmation(alertboxRef)}
					/>
				</AlertBox>
			);
		}
		return null;
	}

	closeConfirmation(alertboxRef) {
		alertboxRef.classList.add("hidden");

		setTimeout(function () {
			alertboxRef.classList.add("display-none");
		}, 500);
	}

	render() {
		if (this.props.hours.length > 0) {
			return (
				<div className="content-container scroll-y content-height gap-20">
					{this.renderConfirmation()}
					<ItemList items={this.props.hours} type="hours" />
				</div>
			);
		} else {
			return <LoadOrError error={this.props.contentError} />;
		}
	}

	componentWillUnmount() {
		this.props.clearError();
	}
}

const mapStateToProps = (state, ownProps) => {
	var hours = state.content.hours;

	if (typeof ownProps.match.params.project !== "undefined") {
		hours = hours.filter(
			(hour) =>
				hour.project_id === parseInt(ownProps.match.params.project)
		);
	}

	// Add project name to worked hours
	hours.forEach((hour) => {
		const project = state.content.projects.find(
			(project) => project.id === hour.project_id
		);

		if (project) {
			hour.project_name = project.project ?? "Project niet gevonden";
		} else {
			hour.project_name = "Project niet gevonden";
		}
	});
	return {
		userKey: state.user.key,
		hours: hours,
		projects: state.content.projects,
		contentError: state.content.error,
	};
};

export default connect(mapStateToProps, {
	getHours,
	setPageTitle,
	getProjects,
	clearError,
})(WorkedHours);
