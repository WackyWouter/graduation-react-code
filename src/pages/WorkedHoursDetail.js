import "./WorkedHoursDetail.css";

import React, { Component } from "react";
import { connect } from "react-redux";

import { getHours, getProjects, getActivities, clearError } from "../actions";
import Button from "../components/Button";
import Back from "../components/Back";
import { ROUTE } from "../constants/routeConstants";
import LoadOrError from "../components/LoadOrError";

class WorkedHoursDetail extends Component {
	componentDidMount() {
		const key = this.props.userKey ?? localStorage.getItem("key");
		if (this.props.hours.length === 0) {
			this.props.getHours(key);
		}
		if (this.props.projects.length === 0) {
			this.props.getProjects(key);
		}
		if (this.props.projects.length === 0) {
			this.props.getActivities(key);
		}
	}

	renderButton() {
		if (this.props.hour.locked) {
			return null;
		}
		return (
			<Button
				text="Bewerk Gewerkte Uren"
				to={ROUTE.EDIT_HOURS + this.props.hour.id}
			/>
		);
	}

	render() {
		if (this.props.hour) {
			const { hour } = this.props;

			const startDate = new Date(hour.start_datetime);
			const endDate = new Date(hour.end_datetime);

			const day = startDate.getDate();
			const year = startDate.getFullYear();
			const month = startDate.toLocaleString("nl-nl", {
				month: "long",
			});

			return (
				<div className="content-container scroll-y content-height hour-detail-container">
					<div className="hour-detail gap-30">
						<Back />
						<div className="form-row">
							<p className="title">Project</p>
							<p className="no-margin">{hour.project_name}</p>
						</div>
						<div className="form-row">
							<p className="title">Datum</p>
							<p className="no-margin">{`${day}th of ${month} ${year}`}</p>
						</div>
						<div className="form-row-time">
							<div className="form-column">
								<p className="title">Start tijd</p>
								<p className="no-margin">
									{startDate.toLocaleTimeString([], {
										hour: "2-digit",
										minute: "2-digit",
									})}
								</p>
							</div>
							<div className="form-column">
								<p className="title">Eind tijd</p>
								<p className="no-margin">
									{endDate.toLocaleTimeString([], {
										hour: "2-digit",
										minute: "2-digit",
									})}
								</p>
							</div>
						</div>
						<div className="form-row">
							<p className="title">Activiteit</p>
							<p className="no-margin">{hour.activity_name}</p>
						</div>
						<div className="form-row">
							<p className="title">Opmerkingen</p>
							<p className="no-margin">{hour.description}</p>
						</div>
					</div>
					{this.renderButton()}
				</div>
			);
		}
		return <LoadOrError error={this.props.contentError} />;
	}
	componentWillUnmount() {
		this.props.clearError();
	}
}

const mapStateToProps = (state, ownProps) => {
	// Get the specific worked hour by filtering with the id form the url
	// Need to use .toString() bcs the id from the url comes back as string
	const hour = state.content.hours.find(
		(hour) => hour.id.toString() === ownProps.match.params.id
	);

	if (hour) {
		// Get the project and activity name associated with the worked hour
		const project = state.content.projects.find(
			(project) => project.id === hour.project_id
		);
		const activity = state.content.activities.find(
			(activity) => activity.id === hour.activity_id
		);

		if (project) {
			hour.project_name = project.project ?? "Project niet gevonden";
		} else {
			hour.project_name = "Project niet gevonden";
		}

		if (activity) {
			hour.activity_name =
				activity.activity ?? "Activiteit niet gevonden";
		} else {
			hour.activity_name = "Activiteit niet gevonden";
		}
	}

	return {
		hour: hour,
		hours: state.content.hours,
		userKey: state.user.key,
		projects: state.content.projects,
		activities: state.content.activities,
		contentError: state.content.error,
	};
};

export default connect(mapStateToProps, {
	getHours,
	getProjects,
	getActivities,
	clearError,
})(WorkedHoursDetail);
