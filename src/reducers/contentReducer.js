import {
	CLEAR_ERROR,
	CLEAR_SEARCH,
	FEEDBACK_SEEN,
	GET_ACTIVITIES,
	GET_FEEDBACK,
	GET_HOURS,
	GET_LEADERBOARD,
	GET_PROJECTS,
	GET_USER_INFO,
	SET_ERROR,
	SET_SEARCH,
	SORT_PROJECTS,
} from "../constants/contentConstants";

const INITIAL_STATE = {
	projects: [],
	filteredProjects: [],
	projectsAToZ: true,
	projectsSearch: null,
	hours: [],
	activities: [],
	leaderboard: [],
	feedback: null,
	userInfo: null,
	error: null,
};

export const contentReducer = (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case SORT_PROJECTS:
			return {
				...state,
				filteredProjects: action.payload.projects,
				projectsAToZ: action.payload.sort,
			};
		case SET_SEARCH: {
			return {
				...state,
				projectsSearch: action.payload.search,
				filteredProjects: action.payload.projects,
			};
		}
		case CLEAR_SEARCH: {
			const filteredProjects = [...state.projects];
			return {
				...state,
				projectsSearch: null,
				filteredProjects: filteredProjects,
			};
		}
		case GET_PROJECTS:
			const filteredProjects = [...action.payload];
			return {
				...state,
				projects: action.payload,
				filteredProjects: filteredProjects,
			};
		case GET_LEADERBOARD:
			return { ...state, leaderboard: action.payload };
		case GET_FEEDBACK:
		case FEEDBACK_SEEN:
			return { ...state, feedback: action.payload };
		case GET_HOURS:
			return { ...state, hours: action.payload };
		case GET_ACTIVITIES:
			return { ...state, activities: action.payload };
		case GET_USER_INFO:
			return { ...state, userInfo: action.payload };
		case CLEAR_ERROR:
			return { ...state, error: null };
		case SET_ERROR:
			return { ...state, error: action.payload };
		default:
			return state;
	}
};
