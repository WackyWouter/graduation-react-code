import {
	CLOSE_MENU,
	OPEN_MENU,
	OPEN_SUG,
	CLOSE_SUG,
	SET_TITLE,
	SET_SUG_DETAILS,
	ACCEPT_SUG,
	CLEAR_SUG_DETAILS,
} from "../constants/displayConstants";

export const displayReducer = (
	state = {
		menuIsOpen: false,
		sugIsOpen: false,
		sugGotDenied: false,
		pageTitle: "Home",
		sugText: "",
		sugProjectId: null,
		sugActivityId: null,
		sugGotAccepted: false,
	},
	action
) => {
	switch (action.type) {
		case CLOSE_MENU:
			return { ...state, menuIsOpen: false };
		case OPEN_MENU:
			return { ...state, menuIsOpen: true };
		case CLOSE_SUG:
			return { ...state, sugIsOpen: false, sugGotDenied: true };
		case OPEN_SUG:
			return { ...state, sugIsOpen: true };
		case SET_TITLE:
			return { ...state, pageTitle: action.payload };
		case SET_SUG_DETAILS:
			return {
				...state,
				sugIsOpen: true,
				sugText: action.payload.text,
				sugProjectId: action.payload.projectId,
				sugActivityId: action.payload.activityId,
			};
		case CLEAR_SUG_DETAILS:
			return {
				...state,
				sugIsOpen: false,
				sugText: null,
				sugProjectId: null,
				sugActivityId: null,
			};
		case ACCEPT_SUG:
			return { ...state, sugGotAccepted: true, sugIsOpen: false };
		default:
			return state;
	}
};
