import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";

import { userReducer } from "./userReducer";
import { contentReducer } from "./contentReducer";
import { displayReducer } from "./displayReducer";

export default combineReducers({
	user: userReducer,
	form: formReducer,
	display: displayReducer,
	content: contentReducer,
});
