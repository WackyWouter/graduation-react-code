import {
	SIGN_IN,
	SIGN_OUT,
	FAILURE,
	FORGOTTEN_PW,
	PASSWORD_RESET,
	CLEAR,
	KEY_UPDATE,
	SET_COORDS,
} from "../constants/userConstants";

const INITIAL_STATE = {
	isSignedIn: null,
	key: null,
	error: null,
	coords: {
		latitude: null,
		longitude: null,
	},
};

export const userReducer = (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case SIGN_IN:
		case PASSWORD_RESET:
			return {
				...state,
				isSignedIn: true,
				key: action.payload.key,
				streak: action.payload.streak,
				error: null,
			};
		case SIGN_OUT:
		case FORGOTTEN_PW:
			return { ...state, isSignedIn: false, userId: null, error: null };
		case FAILURE:
			return {
				...state,
				isSignedIn: false,
				userId: null,
				error: action.payload,
			};
		case KEY_UPDATE:
			return { ...state, key: action.payload };
		case SET_COORDS:
			return {
				...state,
				coords: {
					latitude: action.payload.latitude,
					longitude: action.payload.longitude,
				},
			};
		case CLEAR:
			return { ...state, error: null };

		default:
			return state;
	}
};
